package com.vladtop.android_practice
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val appInfo: String = "\n-${BuildConfig.BASE_URL}\n-${BuildConfig.FLAVOR}\n" +
                "-${BuildConfig.VERSION_NAME}\n-${BuildConfig.BASE_URL}"
        Log.d("MainActivity",appInfo)

    }
}