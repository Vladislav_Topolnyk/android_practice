package com.vladtop.a04

data class Contact(
    val name: String,
    val number: String,
    val organization: String,
    val email: String,
    val id: String
){
    companion object{
        const val DEFAULT_VALUE = "Unknown"
    }
}