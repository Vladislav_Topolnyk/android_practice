package com.vladtop.a04

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.vladtop.a04.Contact.Companion.DEFAULT_VALUE


class MainActivity : AppCompatActivity() {

    private val listView by lazy { findViewById<ListView>(R.id.list_item) }
    private val emailTextView by lazy { findViewById<TextView>(R.id.email) }
    private val organizationTextView by lazy { findViewById<TextView>(R.id.organization) }
    private val nameTextView by lazy { findViewById<TextView>(R.id.name) }
    private val numberTextView by lazy { findViewById<TextView>(R.id.phone_number) }
    private val chargingStateTextView by lazy { findViewById<TextView>(R.id.chargingState) }
    private val contactList = mutableListOf<Contact>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onCreatePermission()
        if (hasReadContactPermission()) {
            formContactList()
            setAdapterToListView()
            listView.onItemClick()
        }
    }

    private fun getData(mimetype: String, id: String): String? {
        val cursor = contentResolver.query(
            ContactsContract.Data.CONTENT_URI, arrayOf(ContactsContract.Data.DATA1),
            "${ContactsContract.Data.MIMETYPE} = ? AND ${ContactsContract.Data.RAW_CONTACT_ID} = ?",
            arrayOf(mimetype, id), null
        )
        return if (cursor?.moveToNext() == true) {
            val data = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Data.DATA1))
            cursor.close()
            data
        } else {
            cursor?.close()
            null
        }
    }

    private fun getIdNumberCursor() = contentResolver.query(
        ContactsContract.Data.CONTENT_URI,
        arrayOf(ContactsContract.Data.DATA1, ContactsContract.Data.RAW_CONTACT_ID),
        "${ContactsContract.Data.MIMETYPE} = ?",
        arrayOf(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE),
        ContactsContract.Data.RAW_CONTACT_ID
    )

    private fun formContactList() {
        getIdNumberCursor().run {
            while (this?.moveToNext() == true) {
                val number = this.getString(this.getColumnIndexOrThrow(ContactsContract.Data.DATA1))
                val id =
                    this.getString(this.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID))
                val name = getData(
                    ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                    id
                ) ?: DEFAULT_VALUE
                val email =
                    getData(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, id)
                        ?: DEFAULT_VALUE
                val organization =
                    getData(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, id)
                        ?: DEFAULT_VALUE

                //add contact to Contact list
                contactList.add(Contact(name, number, organization, email, id))

            }
            this?.close()
        }
    }

    private fun formDataList(dataList: ArrayList<HashMap<String, String>>) {
        contactList.forEach {
            //add elements to list
            val map = HashMap<String, String>()
            map["Name"] = it.name
            map["Number"] = it.number
            dataList.add(map)
            //==================//
        }
    }

    private fun setAdapterToListView() {
        val dataList = ArrayList<HashMap<String, String>>()
        formDataList(dataList)
        val textViews = intArrayOf(android.R.id.text1, android.R.id.text2)
        val adapter = SimpleAdapter(
            this,
            dataList,
            android.R.layout.simple_list_item_2,
            arrayOf("Name", "Number"),
            textViews
        )
        listView.adapter = adapter//set adapter for list view
    }

    private fun ListView.onItemClick() {
        this.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, _, id ->

                val contact = contactList[id.toInt()]

                contact.let {
                    emailTextView.text = it.email
                    organizationTextView.text = it.organization
                    nameTextView.text = it.name
                    numberTextView.text = it.number
                }

                numberTextView.setOnClickListener {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${contact.number}"))
                    startActivity(intent)
                }

                emailTextView.setOnClickListener {
                    val intent = Intent(Intent.ACTION_SENDTO).apply {
                        data = Uri.parse("mailto:") // only email apps should handle this
                        putExtra(Intent.EXTRA_EMAIL, arrayOf(contact.email))
                        putExtra(Intent.EXTRA_SUBJECT, "Hello letter")
                        putExtra(Intent.EXTRA_TEXT, "Hello dude, How are you?")
                    }
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this@MainActivity,
                            "There is no app that support this action",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
    }

    private fun onCreatePermission() {
        if (!hasReadContactPermission()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_CONTACTS
                )
            )
                showAlertDialog()
            else ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CONTACTS),
                0
            )

        }
    }

    private fun showAlertDialog() {
        AlertDialog.Builder(this)
            .setTitle("Read contact permission")
            .setMessage("The program would not work without granted the permission!")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    0
                )
            }
            .show()
    }

    private fun hasReadContactPermission() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED

    private fun onReadContactPermissionGranted() {
        Toast.makeText(this, "Permission was Grant!", Toast.LENGTH_SHORT).show()
    }

    private fun deniedForeverCase() {
        AlertDialog.Builder(this)
            .setTitle("Read Contact Permission")
            .setMessage("You have denied read_contact permission forever!\nGo to settings to change your decision!\n")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                openSettings()
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                finish()
            }
            .show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onReadContactPermissionGranted()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    //return true if permission has been denied
                    //return false if permission has been denied forever or the permission hasn`t been called
                    this,
                    Manifest.permission.CAMERA
                )
            )
                showAlertDialog()
            else deniedForeverCase()
        }
    }

    private fun openSettings() {
        startActivity(
            Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
            )
        )
    }

    private val broadcastReceiver = MyReceiver()

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(ConnectivityManager.ACTION_RESTRICT_BACKGROUND_CHANGED).apply {
            addAction(Intent.ACTION_POWER_DISCONNECTED)
            addAction(Intent.ACTION_POWER_CONNECTED)
        }
        registerReceiver(broadcastReceiver, filter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    inner class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action.equals(Intent.ACTION_POWER_CONNECTED, ignoreCase = true)) {
                chargingStateTextView.text = "Charge is connected"
            } else {
                chargingStateTextView.text = "Charge is not connected"
            }
        }

    }
}
