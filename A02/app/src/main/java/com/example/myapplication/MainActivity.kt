package com.example.myapplication

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.annotation.RequiresApi
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import com.example.myapplication.stuff.CameraActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : CameraActivity() {

    override lateinit var previewView: PreviewView

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        previewView = findViewById(R.id.preview_view)
        onCreatePermission()
    }

    private fun onCreatePermission() {
        if (!hasCameraPermission()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
                showAlertDialog(this)
            else ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 0)

        } else {
            onCameraPermissionGranted()
        }
    }

    private fun hasCameraPermission() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

    private fun onCameraPermissionGranted() {
        Toast.makeText(this, "Camera was Grant!", Toast.LENGTH_SHORT).show()
        launchCamera()
    }

    private fun deniedForeverCase() {
        AlertDialog.Builder(this)
            .setTitle("Camera Permission")
            .setMessage("You have denied camera permission forever!\nGo to settings to change your decision!\n")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                openSettings()
            }
            .setNegativeButton(android.R.string.no) { dialog, _ -> dialog.cancel() }
            .show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onCameraPermissionGranted()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
                showAlertDialog(this)
            else deniedForeverCase()
        }
    }
}
