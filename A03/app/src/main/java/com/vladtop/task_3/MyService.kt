package com.vladtop.task_3

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log

class MyService : Service() {
    var howManyTimesWasAccessed = 0
    get() = ++field

    override fun onBind(intent: Intent?): IBinder {
        return MyBinder()
    }

    inner class MyBinder : Binder() {
        fun getService(): MyService = this@MyService
    }

}