package com.vladtop.task_3


import android.os.Bundle
import android.widget.Button

class Activity3 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_3)
        val exitButton = findViewById<Button>(R.id.exit3)
        val activityButton = findViewById<Button>(R.id.button_toActivity4)
        exitButton.setOnClickListener { backButtonClicked(this) }
        activityButton.setOnClickListener { forwardButtonClicked(this, Activity4()) }
    }
}