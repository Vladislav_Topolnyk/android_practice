package com.vladtop.task_3


import android.content.ContextWrapper
import android.content.Intent

import androidx.appcompat.app.AppCompatActivity


open class BaseActivity : AppCompatActivity() {
    protected fun backButtonClicked(activity: BaseActivity) {
        activity.finish()
    }

    protected open fun forwardButtonClicked(context: ContextWrapper, activity: AppCompatActivity) {
        val intent = Intent(context, activity::class.java)
        startActivity(intent)
    }
}