package com.vladtop.task_3

import android.os.Bundle

import android.widget.Button


class Activity1 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        val exitButton = findViewById<Button>(R.id.exit)
        val activityButton = findViewById<Button>(R.id.button_toActivity2)
        exitButton.setOnClickListener { backButtonClicked(this) }
        activityButton.setOnClickListener { forwardButtonClicked(this, Activity2()) }
    }
}