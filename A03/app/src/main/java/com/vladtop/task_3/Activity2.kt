package com.vladtop.task_3


import android.os.Bundle
import android.util.Log
import android.widget.Button

class Activity2 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)
        val exitButton = findViewById<Button>(R.id.exit2)
        val activityButton = findViewById<Button>(R.id.button_toActivity3)
        exitButton.setOnClickListener { backButtonClicked(this) }
        activityButton.setOnClickListener { forwardButtonClicked(this, Activity3()) }
        Log.d("Current context", baseContext.toString())
    }
}