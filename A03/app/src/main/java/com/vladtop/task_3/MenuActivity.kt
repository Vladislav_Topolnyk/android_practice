package com.vladtop.task_3

import android.os.Bundle
import android.widget.Button

class MenuActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val firstPartButton = findViewById<Button>(R.id.FirstPartButton)
        val secondPartButton = findViewById<Button>(R.id.SecondPartButton)
        firstPartButton.setOnClickListener { forwardButtonClicked(this, Activity1()) }
        secondPartButton.setOnClickListener { forwardButtonClicked(this, ServiceActivity()) }
    }
}