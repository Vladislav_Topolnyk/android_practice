package com.vladtop.task_3

import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(LifeCycleCallBacksImpl())
    }

}