package com.vladtop.task_3

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log

class LifeCycleCallBacksImpl : Application.ActivityLifecycleCallbacks {
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        Log.d("On Activity created", activity.localClassName)
    }

    override fun onActivityStarted(activity: Activity) {
        Log.d("On Activity started", activity.localClassName)
    }

    override fun onActivityResumed(activity: Activity) {
        Log.d("On Activity resumed", activity.localClassName)
    }

    override fun onActivityPaused(activity: Activity) {
        Log.d("On Activity paused", activity.localClassName)
    }

    override fun onActivityStopped(activity: Activity) {
        Log.d("On Activity stopped", activity.localClassName)
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        Log.d("On Activity SaveInstanceState", activity.localClassName)
    }

    override fun onActivityDestroyed(activity: Activity) {
        Log.d("On Activity destroyed", activity.localClassName)
    }
}