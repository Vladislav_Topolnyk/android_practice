package com.vladtop.task_3

import android.os.Bundle
import android.widget.Button

class Activity4 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_4)
        val exitButton = findViewById<Button>(R.id.exit4)
        val activityButton = findViewById<Button>(R.id.button_toActivity1)
        exitButton.setOnClickListener { backButtonClicked(this) }
        activityButton.setOnClickListener { forwardButtonClicked(this, Activity1()) }
    }
}