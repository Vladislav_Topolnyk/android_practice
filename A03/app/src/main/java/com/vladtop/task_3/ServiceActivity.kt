package com.vladtop.task_3

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ServiceActivity : AppCompatActivity() {
    lateinit var mService: MyService
    var mBound: Boolean = false
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as MyService.MyBinder
            mService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)
        val intent = Intent(this, MyService::class.java)
        startService(intent)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
        val counterButton = findViewById<Button>(R.id.CounterButton)
        counterButton.setOnClickListener { onCounterButtonClicked() }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        mBound = false
    }

    private fun onCounterButtonClicked() {
        Toast.makeText(
            this,
            "Was requested ${mService.howManyTimesWasAccessed} times",
            Toast.LENGTH_SHORT
        ).show()
    }
}