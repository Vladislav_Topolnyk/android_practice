package com.vladtop.testfragment

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

class Fragment1 : Fragment() {
    interface OnFragmentSendDataListener {
        fun changeColor()
        fun replaceFragments()
        fun openDialogFragment()
    }

    private lateinit var onFragmentSendDataListener: OnFragmentSendDataListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            onFragmentSendDataListener = context as OnFragmentSendDataListener
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString()
                        + " должен реализовывать интерфейс OnFragmentInteractionListener"
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("fragment1", "onCreateView")
        return inflater.inflate(R.layout.fragment_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val buttonChangeColor = view.findViewById<Button>(R.id.buttonChangeColor)
        val buttonReplaceFragment2 = view.findViewById<Button>(R.id.buttonReplaceFragment2)
        val createListButton = view.findViewById<Button>(R.id.create_list_button)
        createListButton.setOnClickListener {
            onFragmentSendDataListener.openDialogFragment()
        }
        buttonChangeColor.setOnClickListener {
            onFragmentSendDataListener.changeColor()
        }
        buttonReplaceFragment2.setOnClickListener {
            onFragmentSendDataListener.replaceFragments()
        }

    }

}