package com.vladtop.testfragment


import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.OutcomeReceiver
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider


class FragmentDialog : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val myView = layoutInflater.inflate(R.layout.dialog_fragment, null)
        val editText = myView.findViewById<EditText>(R.id.editText)
        val myViewModel = ViewModelProvider(requireActivity()).get(MyViewModel::class.java)

        val dialog = AlertDialog.Builder(this.requireContext()).setView(myView)
            .setTitle("Create list").setPositiveButton("Create") { myDialog, _ ->
                myViewModel.sendData(editText.text)
                myDialog.cancel()
            }.setNegativeButton("Cancel") { myDialog, _ ->
                myDialog.cancel()
            }.create()

        dialog.setCanceledOnTouchOutside(false)

        dialog.show()

        editText.makeButtonEnabled(dialog.getButton(DialogInterface.BUTTON_POSITIVE))

        return dialog
    }

    private fun EditText.makeButtonEnabled(createButton: Button) {
        createButton.isEnabled = false
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                createButton.isEnabled = this@makeButtonEnabled.text.length > 3
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

    }
}
