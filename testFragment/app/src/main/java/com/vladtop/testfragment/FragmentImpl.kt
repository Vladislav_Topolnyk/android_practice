package com.vladtop.testfragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment

open class FragmentImpl : Fragment() {
    companion object {
        const val COLOR_KEY = "COLOR"
    }

    protected var backGroundColor: Int = 0
    protected lateinit var textView: TextView
    private val className = this::class.java.simpleName

    fun setColor(color: Int) {
        backGroundColor = color
        textView.setBackgroundColor(backGroundColor)
    }

    fun getColor() = backGroundColor

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(className, "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val color = savedInstanceState?.getInt(COLOR_KEY)
        if (color != null) {
            backGroundColor = color
        }
        Log.d(className, "onCreate")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (backGroundColor != 0) {
            textView.setBackgroundColor(backGroundColor)
        } else backGroundColor = (textView.background as ColorDrawable).color

        Log.d(className, "onViewCreated")
    }

    override fun onStart() {
        super.onStart()
        Log.d(className, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(className, "onResume")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(className, "onSaveInstanceState")
        outState.putInt(COLOR_KEY, backGroundColor)
    }

    override fun onPause() {
        super.onPause()
        Log.d(className, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(className, "onStop")

    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(className, "onDestroyView")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(className, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(className, "onDetach")
    }

}
