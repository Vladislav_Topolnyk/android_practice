package com.vladtop.testfragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider


class Fragment2 : FragmentImpl() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_2, container, false)
        val dataView = view.findViewById<TextView>(R.id.receive_data)
        textView = view.findViewById(R.id.fragment2_textView)

        dataView.getDataFromViewModel()

        Log.d(this::class.java.simpleName, "onCreateView")
        return view
    }

    private fun TextView.getDataFromViewModel() {
        val myViewModel = ViewModelProvider(requireActivity()).get(MyViewModel::class.java)
        myViewModel.getData().observe(viewLifecycleOwner, Observer {
            this.text = it
        })
    }
}
