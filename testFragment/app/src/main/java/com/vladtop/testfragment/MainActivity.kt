package com.vladtop.testfragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity(), Fragment1.OnFragmentSendDataListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            addFragment(R.id.fragment_container_controller,Fragment1())
            addFragment(R.id.fragmentContainerViewLeft, Fragment2())
            addFragment(R.id.fragmentContainerViewRight, Fragment3())
        }
    }

    private fun addFragment(containerView: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().addToBackStack(null)
            .add(containerView, fragment)
            .commit()
    }

    private fun initFragment(containerView: Int) =
        supportFragmentManager.findFragmentById(containerView) as FragmentImpl


    override fun changeColor() {
        val fragmentLeft = initFragment(R.id.fragmentContainerViewLeft)
        val fragmentRight = initFragment(R.id.fragmentContainerViewRight)

        val fragmentColorLeft = fragmentLeft.getColor()
        val fragmentColorRight = fragmentRight.getColor()

        fragmentLeft.setColor(fragmentColorRight)
        fragmentRight.setColor(fragmentColorLeft)
    }

    override fun replaceFragments() {
        val fragmentLeft = initFragment(R.id.fragmentContainerViewLeft)
        val fragmentRight = initFragment(R.id.fragmentContainerViewRight)

        val copyFragmentLeft = copyFragmentWithState(fragmentLeft)
        val copyFragmentRight = copyFragmentWithState(fragmentRight)

        replaceFragment(copyFragmentLeft, R.id.fragmentContainerViewRight)
        replaceFragment(copyFragmentRight, R.id.fragmentContainerViewLeft)
    }

    private fun replaceFragment(
        newInstance: FragmentImpl,
        containerView: Int
    ) {
        supportFragmentManager.beginTransaction()
            .replace(containerView, newInstance)
            .commit()
        supportFragmentManager.executePendingTransactions()

    }

    override fun openDialogFragment() {
        FragmentDialog().show(supportFragmentManager, "dialog")
    }

    private fun copyFragmentWithState(oldFragment: FragmentImpl): FragmentImpl {
        val oldState: Fragment.SavedState? =
            supportFragmentManager.saveFragmentInstanceState(oldFragment)
        val newInstance = oldFragment.javaClass.newInstance()
        newInstance.setInitialSavedState(oldState)
        return newInstance
    }
}