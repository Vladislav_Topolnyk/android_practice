package com.vladtop.testfragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MyViewModel : ViewModel() {
    private val mutableLiveData: MutableLiveData<CharSequence> = MutableLiveData()


    fun sendData(input: CharSequence) {
        mutableLiveData.value = input
    }


    fun getData(): MutableLiveData<CharSequence> = mutableLiveData
}